# ruNCGR – **r**4dh4l’s **u**ltimate **N**ecromunda **C**ampaign **G**ang **R**oster

A [tabletop](https://en.wikipedia.org/wiki/Miniature_wargaming) project for a better [Necromunda](https://en.wikipedia.org/wiki/Necromunda) Gang Roster.

[![runcgr_preview](media/runcgr_preview.png "ruNCFC preview picture")](https://gitlab.com/r4dh4l/runcgr/-/blob/main/runcgr.pdf)

# General idea

The idea of this Necromunda Gang Roster is to have better track possibilities for your Necromunda Gang progress during a campaign, for example:
- to use the space of the sheet more efficiently in comparison to the original Gang Roster,
- to track played matches,
- to reduce redundant information in comparison to the original Gang Roster which wants you to write down a lot of things you already wrote on the Fighter Cards.
- to track the credit cashflow with a special table, see `runcgr_cashflow.pdf`.

This Gang Roster is intended to be used in combination with copies of the [ruNCFC](https://gitlab.com/r4dh4l/runcfc/) (**r**4dh4l’s **u**ltimate **N**ecromunda **C**ampaign **F**ighter **C**ards).

You may notice some additional fields in comparison to the original Gang Roster like `Gang ID`, `Fighter ID`, `Arena Gang Glory` and `AT` the author needed for a campaign for which the author created this Gang Roster. To explain at least two of them:
- The `Gang ID` is a short version of your Gang name, maybe an acronym of it. By numbering your Gang fighters and combine it with the Gang ID each Gang fighter gets a unique ID, the `Fighter ID`. For example: If you are in a rush and have to recruit a new Gang fighter and have not a good idea for a Fighter name right now you already have an ID for the Fighter so you can name the Fighter later. Or if you capture an opponent Gang fighter you can just ask for the Fighter ID instead of writing down crazy, long Fighter names.
- The `Arena Gang Glory` is a way to track the success of the Gang's Fighting Pit matches. You maybe don't need this field and can just ignore or modify it for your demands.

# Purpose of the files

## [runcgr.pdf](https://gitlab.com/r4dh4l/runcgr/-/blob/main/runcgr.pdf)

**This is the file you surely want to use in a Necromunda campaign printing it and filling it out by hand (I suggest using a pencil).** It is a PDF export from `runcgr.odt`. Unfortunately it is not digitally fillable yet.

## [runcgr.odt](https://gitlab.com/r4dh4l/runcgr/-/blob/main/runcgr.odt)

`runcgr.odt` can be used
- to adjust the file for your demands before printing it or
- to export the file to PDF files different to the one provided here.

It is an **O**pen**D**ocument**T**ext formated file created with [LibreOffice](https://www.libreoffice.org/) (you can use any other office program which can handle ODT to edit the file but please share derivates of the file as ODT as well so that a freedom respecting handling of the file is ongoing possible). 

To export a modified `runcgr.odt` as 4on1 PDF (like `runcgr_4on1.pdf`) you have to print the ODT file as a file using the following settings under `File` -> `Print` -> `General` tab:
- Section "Printer": `Print to Files...`
- Section "Range and Copies" -> "Pages": `1,1,1,1`
- Section "Page Layout" -> "more" -> "Pages per sheet": `4`

Please keep in mind to respect the [license](https://gitlab.com/r4dh4l/runcgr/-/blob/main/LICENSE.md) making your modifications. You are very welcome to use (even commercially), share and improve this document as long as you grant others the same to your adaptions (means: you are not allow to change the license CC BY-SA).

## [runcgr_cashflow.pdf](https://gitlab.com/r4dh4l/runcgr/-/blob/main/runcgr.pdf)

Inspired by the board game "Cashflow" this table provides a way to protocol your spendings and earnings. Just write down any amount of Credits and for what you spent or got it. It may be a little bit exhausting in the beginning but this way you always know how you run out of Credits and can explain your Wealth value to others. ;) You can modify it with [runcgr_cashflow.odt](https://gitlab.com/r4dh4l/runcgr/-/blob/main/runcgr_cashflow.odt) in the same way as described for `runcgr.odt`:

[![runcgr_cashflow_preview](media/runcgr_cashflow_preview.png "ruNCFC cashflow preview picture")](https://gitlab.com/r4dh4l/runcgr/-/blob/main/runcgr_cashflow.pdf)
